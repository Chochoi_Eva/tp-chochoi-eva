carteLeaflet = new L.Map("map-hydrant", {
    center: [50.6357, 3.05833],
    zoom: 13
});
new L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(carteLeaflet);