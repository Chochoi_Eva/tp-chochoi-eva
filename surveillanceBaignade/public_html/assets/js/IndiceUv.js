var http = require('http');
const request = require('request');     //module permettant les requêtes http 
const config = require('./config');

var Si1145 = require('@agilatech/si1145');  //inclus le module si1145
var si1145 = new Si1145();

// Envoi une mesure d'UV via le service REST en POST

function sendIndexUV(prmIndexUV){
    request.post({
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        url:`http://${config.rest.host}/surveillanceBaignade/index.php/REST/C_indiceUv/index/`,
        body: `dto={"iuv": ${prmIndexUV}, "date" : "01-02-2020", "IDplage" : "1"}`
    }, (error, reponse, body) => console.log(body));
}

if (si1145.deviceActive()) {
    si1145.getDataFromDevice((err) => {     //fonction de callback
        if (!err) {                         //boucle qui permet de détecter si il y a des erreurs 
            const uvIdx   = si1145.device.parameters[2].value;
            console.log("indiceUv :"+uvIdx);
            sendIndexUV(uvIdx);    //definition d'un champ de formulaire s'effectue à l'aide de la méthode send()
        }
    });
}
setInterval(sendIndexUV,1800000); /*
module.exports = {
    sendIndexUV
};
*/

