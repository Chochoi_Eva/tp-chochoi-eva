</nav>
<section class="ml-5 mt-5">
    <div class="card border-warning mb-3 mt-5 rounded mx-auto d-block" style="max-width: 50rem; max-height: 50rem;">
        <div class="card-header display-4 font-italic">
            <center>Votre temps d'exposition au soleil</center>
        </div>
    </div>
    <div class="mt-5 ml-5" style="max-width: 50rem; max-height: 50rem; font-size : 150px;">
    <p class="display-2 text-center"> Votre temps d'exposition au soleil est de <i><u>30 mins</u></i> avec une indice de protection solaire de <i><u>20</u></i>. </p>
    <img src="<?php echo base_url("assets/images/bronzette.png"); ?>" alt="bronzette">
    </div>
</section>