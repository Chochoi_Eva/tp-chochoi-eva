<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_detail_Meteo extends CI_Controller {
    public  function __construct() {
        parent::__construct();
        $this->load->model('M_detail_Meteo');
         $this->load->helper("url");
    }

    public function index_liste_plage()
    {
        $data['titre'] = "La liste des plages de la communes";
        $data['Titre_Page'] = "Les plages";

        $array_resultat = $this->M_indiceUv->select_liste_plage();
        $data['result'] = $array_resultat;
        $page = $this->load->view('V_liste_plage', $data, true);
        $this->load->view('commun/V_template', array('contenu' => $page));
    }

    public function index_Meteo_Plage($prmID)
    {
        //nom de la plage 
        $array_resultat = $this->M_detail_Meteo->select_nom_plage($prmID);
        $data['resultat'] = $array_resultat;
        //temperature de l'eau 
        $array_resultats = $this->M_detail_Meteo->select_temp_eau($prmID); 
        $data['result'] = $array_resultats;
        //temperature de l'air
        $resultats = $this->M_detail_Meteo->select_temp_air($prmID); 
        $data['results'] = $resultats;
        //direction et vitesse du vent
        $LesResultats = $this->M_detail_Meteo->select_direction_vent($prmID); 
        $data['resultats'] = $LesResultats;

        $data['titre'] = "La liste des plages de la communes";
        
        $page = $this->load->view('V_plage_meteo', $data, true);
        $this->load->view('commun/V_template', array('contenu' => $page)); 
    }

}