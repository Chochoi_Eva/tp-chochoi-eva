<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_tempsExpo extends CI_Controller {
    public  function __construct() {
        parent::__construct();
         $this->load->helper("url");
    }

    public function index_tempsExpo()
    {
        $data['titre'] = "La liste des plages de la communes";
        $data['Titre_Page'] = "Les plages";

        $page = $this->load->view('V_tempsExpo', $data, true);
        $this->load->view('commun/V_template', array('contenu' => $page));
    }
}