<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_calcul_expo extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function select_nom_plage($prmID)
    {
        $query = $this->db->select('nom')
            ->from('plage')
            ->where('IDplage', $prmID)
            ->get();
        return $query->result_array();
    }

    public function select_indice_uv($prmID)
    {
        $query = $this->db->select('*')
            ->from('mesuv')
            ->where('IDplage', $prmID)
            ->get();
        return $query->result_array();
    }
}
