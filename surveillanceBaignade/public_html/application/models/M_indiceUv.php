<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_indiceUv extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function select_all_plage()
    {
        $query = $this->db->select('nom')
                          ->from('plage')
                          ->get();
        return $query->result_array();
    }

    public function select_all_MessagePerso()
    {
        $query = $this->db->select('message')
                          ->from('messageperso')
                          ->get();
        return $query->result_array();
    }

    public function select_liste_plage()
    {
        $query = $this->db->select('IDplage, nom')
                          ->from('plage')
                          ->get();
        return $query->result_array();
    }
}